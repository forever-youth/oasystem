package com.dao;

import com.entity.Employee;
import com.entity.Note;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Admin
 */
@Repository
public interface NoteDao {

    /**
     *  findByNotePage 查询全部信息
     * @param eid
     * @return
     */
    List<Note> findByNotePage(int eid);

    /**
     * addNote 申请请假
     * @param note
     * @return
     */
    int addNote(Note note);

    /**
     * 通过当前账户权限查询信息
     * @param map
     * @return
     */
    List<Integer> findIdsByDid(HashMap<String,Object> map);

    /**
     * 除了boss以外的数据
     * @param map
     * @return
     */
    List<Integer> findIdsByDidPo(HashMap<String,Object> map);

    /**
     * findNoteByIds
     * @param map
     * @return
     */
    List<Note> findNoteByIds(HashMap<String,Object> map);

    /**
     * 同意、打回、不同意
     * @param map
     * @return
     */
    int updateNoteEstatus(HashMap<String,Object> map);

    /**
     * 根据nid查询Employee信息
     * @param nid
     * @return
     */
    Employee findEmpNoteByNid(@Param("nid") int nid);

    /**
     * 根据nid查询Note信息
     * @param nid
     * @return
     */
    Note findNoteByNid(@Param("nid") int nid);

    /**
     * 修改信息
     * @param note
     * @return
     */
    int editNote(Note note);

}

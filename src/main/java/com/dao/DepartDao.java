package com.dao;

import com.entity.Depart;
import com.entity.Employee;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author Lenovo
 */
public interface DepartDao {

    /**
     *查询部门
     *
     * @param dstatus
     * @return 所有部门信息
     */
    List<Depart> getAllDepart(int dstatus);

    /**
     *添加部门
     *
     * @param dName
     * @param duty
     * @return 1 , 0
     */
    int addDept(@Param("dName") String dName , @Param("duty") String duty);

    /**
     *跳转到修改页面
     *
     * @param dId
     * @return 部门信息
     */
    Depart findDeptById(int dId);

    /**
     *修改部门
     *
     * @param depart
     * @return 1 , 0
     */
    int editDept(Depart depart);

    /**
     *修改部门状态
     *
     * @param depart
     * @return 1 , 0
     */
    int editDStatusDept(Depart depart);

}

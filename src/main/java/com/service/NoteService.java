package com.service;

import com.entity.Employee;
import com.entity.Note;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.HashMap;
import java.util.List;

/**
 * @author Admin
 */
public interface NoteService {

    /**
     * findByNoteAll 查询全部信息
     * @param eid
     * @return
     */
    List<Note> findByNotePage(int eid);

    /**
     * addNote 申请请假
     * @param note
     * @return
     */
    int addNote(Note note);

    /**
     *findIdsByDid
     * @param did
     * @param position
     * @return
     */
    List<Integer> findIdsByDid(int did, int position);

    /**
     * findNoteByIds遍历查询
     * @param ids
     * @param type
     * @return
     */
    List<Note> findNoteByIds(List<Integer> ids, int type);

    /**
     * 同意、打回、不同意
     * @param nid
     * @param eStatus
     * @return
     */
    int updateNoteEstatus(int nid,int eStatus);

    /**
     * 根据nid查询信息
     * @param nid
     * @return
     */
    Employee findEmpNoteByNid(@Param("nid") int nid);

    /**
     * 根据nid查询Note信息
     * @param nid
     * @return
     */
    Note findNoteByNid(@Param("nid") int nid);

    /**
     * 修改信息
     * @param note
     * @return
     */
    int editNote(Note note);
}

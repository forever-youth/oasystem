package com.service;

import com.entity.Depart;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * @author Lenovo
 */
public interface DepartService {
    /**
     *查询部门
     *
     * @param dstatus
     * @return Employee 登录员工信息
     */
    List<Depart> getAllDepart(int dstatus);

    /**
     *添加部门
     *
     * @param dName
     * @param duty
     * @return Employee 登录员工信息
     */
    int addDept(@Param("dName") String dName , @Param("duty") String duty);

    /**
     *跳转到修改页面
     *
     * @param dId
     * @return 部门信息
     */
    Depart findDeptById(int dId);

    /**
     *修改部门
     *
     * @param depart
     * @return 1 , 0
     */
    int editDept(Depart depart);

    /**
     *修改部门状态
     *
     * @param depart
     * @return 1 , 0
     */
    int editDStatusDept(Depart depart);
}

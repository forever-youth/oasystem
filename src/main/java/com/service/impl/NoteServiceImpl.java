package com.service.impl;

import com.dao.NoteDao;
import com.entity.Employee;
import com.entity.Note;
import com.service.NoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Admin
 */
@Service
public class NoteServiceImpl implements NoteService {

    @Autowired
    private NoteDao noteDao;

    @Override
    public List<Note> findByNotePage(int eid) {
        return noteDao.findByNotePage(eid);
    }

    @Override
    public int addNote(Note note) {
        return noteDao.addNote(note);
    }

    @Override
    public List<Integer> findIdsByDid(int did, int position) {
        HashMap<String,Object> map = new HashMap<>();
        map.put("did",did);
        map.put("position",position);

        if (did == 1){
            return noteDao.findIdsByDid(map);
        }else {
            return noteDao.findIdsByDidPo(map);
        }
    }

    @Override
    public List<Note> findNoteByIds(List<Integer> ids, int type) {
        HashMap<String,Object> map = new HashMap<>();
        map.put("ids",ids);
        map.put("type",type);
        return noteDao.findNoteByIds(map);
    }

    @Override
    public int updateNoteEstatus(int nid, int eStatus) {
        HashMap<String,Object> map = new HashMap<>();
        map.put("nid",nid);
        map.put("eStatus",eStatus);
        return noteDao.updateNoteEstatus(map);
    }

    @Override
    public Employee findEmpNoteByNid(int nid) {
        return noteDao.findEmpNoteByNid(nid);
    }

    @Override
    public Note findNoteByNid(int nid) {
        return noteDao.findNoteByNid(nid);
    }

    @Override
    public int editNote(Note note) {
        return noteDao.editNote(note);
    }
}

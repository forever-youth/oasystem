package com.service.impl;

import com.dao.DepartDao;
import com.entity.Depart;
import com.service.DepartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Lenovo
 */
@Service
public class DepartServiceImpl implements DepartService {

    @Autowired
    DepartDao dao;

    @Override
    public List<Depart> getAllDepart(int dStatus) {
        return dao.getAllDepart(dStatus);
    }

    @Override
    public int addDept(String dName, String duty) {
        return dao.addDept(dName,duty);
    }

    @Override
    public Depart findDeptById(int dId) {
        return dao.findDeptById(dId);
    }

    @Override
    public int editDept(Depart depart) {
        return dao.editDept(depart);
    }

    @Override
    public int editDStatusDept(Depart depart) {
        return dao.editDStatusDept(depart);
    }

}

package com.service.impl;

import com.dao.EmployeeDao;
import com.entity.Employee;
import com.service.EmployeeService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Lenovo
 */
@Service
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    private EmployeeDao eDao;

    @Override
    public Employee login(String eName, String ePass) {
        return eDao.login(eName,ePass);
    }

    @Override
    public String eNameIf(String eName) {
        return eDao.eNameIf(eName);
    }

    @Override
    public int updatePwd(Employee employee) {
        return eDao.updatePwd(employee);
    }

    @Override
    public int selectCount(int eStatus,String realName) {
        return eDao.selectCount(eStatus,realName);
    }

    @Override
    public List<Employee> findEmpByPage(@Param("eStatus") int eStatus,@Param("page") int page,@Param("length") int length,@Param("realName")String realName) {
        Map map = new HashMap(length);
        map.put("page",page);
        map.put("length",length);
        map.put("eStatus",eStatus);
        map.put("realName",realName);
        return eDao.findEmpByPage(map);
    }

    @Override
    public int deleteUser(Employee employee) {
        return eDao.deleteUser(employee);
    }

    @Override
    public int addUser(Employee employee) {
        return eDao.addUser(employee);
    }

    @Override
    public Employee findEmployeeById(int eid) {
        return eDao.findEmployeeById(eid);
    }

    @Override
    public int editUser(Employee employee) {
        return eDao.editUser(employee);
    }

    @Override
    public List<Employee> findEmpAll(int eStatus) {
        return eDao.findEmpAll(eStatus);
    }


}

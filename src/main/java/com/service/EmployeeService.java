package com.service;

import com.entity.Employee;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author Lenovo
 */
public interface EmployeeService {

    /**
     * 登录.
     * @param eName 登录账号
     * @param ePass 登录密码
     * @return Employee 登录员工信息
     */
    Employee login(@Param("eName") String eName, @Param("ePass") String ePass);

    /**
     * 判断账号存不存在
     * @param eName 员工账号
     * @return 员工账号
     */
    String eNameIf(String eName);

    /**
     * 通过id修改密码
     * @param employee
     * @return 修改成功（1），失败（0）
     */
    int updatePwd(Employee employee);

    /**
     *通过员工状态查询总数
     *
     * @param eStatus
     * @param realName
     * @return 总数
     */
    int selectCount(int eStatus,String realName);

    /**
     *通过员工状态查询员工信息并分页
     *
     * @param eStatus
     * @param page
     * @param length
     * @return 员工信息
     */
    List<Employee> findEmpByPage(@Param("eStatus") int eStatus,@Param("page") int page,@Param("length") int length,@Param("realName") String realName);

    /**
     * deleteUser 根据id删除信息
     * @param employee
     * @return
     */
    int deleteUser(Employee employee);

    /**
     * 新增信息
     * @param employee
     * @return
     */
    int addUser(Employee employee);

    /**
     * 通过id查询信息
     * @param eid
     * @return
     */
    Employee findEmployeeById(int eid);

    /**
     * editUser 根据id修改信息
     * @param employee
     * @return
     */
    int editUser(Employee employee);

    /**
     * findEmpAll 通过状态查询全部信息
     * @param eStatus
     * @return
     */
    List<Employee> findEmpAll(int eStatus);

}

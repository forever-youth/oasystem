package com.controller;

import com.entity.Employee;
import com.entity.Note;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.service.EmployeeService;
import com.service.NoteService;
import com.vo.NoteVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * NoteController
 * @author Admin
 */
@Controller
public class NoteController {

    @Autowired
    private NoteService noteService;

    @Autowired
    private EmployeeService employeeService;

    /**
     * 查询请假信息
     * @param model
     * @param session
     * @param page
     * @param length
     * @param type
     * @param eid
     * @return
     */
    @RequestMapping("/findNoteByPage")
    public String findNoteByPage(Model model,HttpSession session, int page, int length, int type, int eid){
        model.addAttribute("type",type);
        if (type == 0){
            PageHelper.startPage(page,length);
            List<Note> notes = noteService.findByNotePage(eid);
            PageInfo<Note> pageInfo = new PageInfo<>(notes);
            model.addAttribute("pageInfo",pageInfo);
        }else {
            PageHelper.startPage(page,length);
            //获取当前登录账户的用户信息
            Employee emp = (Employee) session.getAttribute("emp");
            //通过当前账户权限查询信息
            List<Integer> ids = noteService.findIdsByDid(emp.getDid(),emp.getPosition());
            //根据获取到的did和type拿到对应的请假记录
            List<Note> notes = noteService.findNoteByIds(ids,type);
            //将请假记录赋值到vo视图中
            List<NoteVo> noteVos = copyBean(notes);
            //存储分页信息
            PageInfo pageInfo = new PageInfo(noteVos);
            model.addAttribute("pageInfo",pageInfo);
        }
        return "leave.jsp";
    }

    /**
     * vo存储展示
     * @param notes
     * @return
     */
    private List<NoteVo> copyBean(List<Note> notes){
        List<NoteVo> noteVos = new ArrayList<>();
        for (Note note : notes){
            Employee employee = employeeService.findEmployeeById(note.getEid());
            NoteVo noteVo = new NoteVo();
            BeanUtils.copyProperties(note,noteVo);
            noteVo.setRealName(employee.getRealName());

            noteVos.add(noteVo);
        }
        return noteVos;
    }

    /**
     * 新增申请信息
     * @param note
     * @return
     */
    @RequestMapping("/addNote")
    public String addNote(Note note){
        noteService.addNote(note);
        return "redirect:/findNoteByPage?page=1&length=4&type=0&eid=1";
    }

    /**
     * 同意、打回、不同意
     * @param nid
     * @param estatus
     * @param eid
     * @return
     */
    @RequestMapping("/updateNoteEstatus")
    public String updateNoteEstatus(int nid,int estatus,int eid){
        /**
         * 同意estatus = 1
         * 打回estatus = 3
         * 不同意 eStatus = 2
         */
        noteService.updateNoteEstatus(nid,estatus);
        return "redirect:/findNoteByPage?page=1&length=4&type=0&eid=1";
    }

    /**
     * 打开编辑弹窗显示内容
     * @param nid
     * @param model
     * @return
     */
    @RequestMapping("/findNoteByNid")
    public String findNoteByNid(int nid, Model model){
        Employee emp = noteService.findEmpNoteByNid(nid);
        Note note = noteService.findNoteByNid(nid);
        model.addAttribute("emp",emp);
        model.addAttribute("note",note);

        return "editLeave.jsp";
    }

    /**
     * 编辑修改内容
     * @param note
     * @return
     */
    @RequestMapping("/editNote")
    public String editNote(Note note){
        noteService.editNote(note);
        return "redirect:/findNoteByPage?page=1&length=4&type=0&eid=1";
    }
}

package com.controller;

import com.entity.Depart;
import com.service.DepartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * @author Lenovo
 */
@Controller
public class DepartController {

    @Autowired
    DepartService service;

    @RequestMapping("/findAllDept")
    public String findAllDept(int dstatus, HttpSession session){
        List<Depart> dep = service.getAllDepart(dstatus);
        session.setAttribute("dep",dep);
        return "depart.jsp";
    }

    @RequestMapping("/addDept")
    public String addDept(String dName ,String duty){
        service.addDept(dName,duty);
        return "findAllDept?dstatus=0";
    }

    /**
     * 跳转到编辑部门页面
     * @param DId 部门编号
     * @return 员工账号
     */
    @RequestMapping("/findDeptById")
    public String findDeptById(int DId , Model model){
        Depart depart = service.findDeptById(DId);
        model.addAttribute("dept",depart);
        return "editDepart.jsp";
    }

    /**
     * 实现编辑部门
     * @param depart 部门名称
     * @return 员工账号
     */
    @RequestMapping("/editDept")
    public String editDept(Depart depart){
        service.editDept(depart);
        return "findAllDept?dstatus=0";
    }

    /**
     * 修改部门状态
     * @param did 部门编号
     * @param dstatus 部门状态
     * @return
     */
    @RequestMapping("/deleteDepart")
    public String editDStatusDept(int did,int dstatus){
        Depart depart=new Depart();
        depart.setDId(did);
        depart.setDStatus(dstatus);
        service.editDStatusDept(depart);
        return "findAllDept?dstatus=0";
    }

}

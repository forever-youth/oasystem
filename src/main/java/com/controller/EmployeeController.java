package com.controller;

import com.entity.Depart;
import com.entity.Employee;
import com.service.DepartService;
import com.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Lenovo
 */
@Controller
public class EmployeeController {

    Pattern p = Pattern.compile("\\W");

    @Autowired
    private EmployeeService service;

    @Autowired
    private DepartService departService;

    /**
    * 登录
    * */
    @RequestMapping("/login")
    public String login(String eName, String ePass, Model model, HttpSession session){
        int elen=eName.trim().length();
        int plen=ePass.trim().length();
        if (elen>= 2 && elen<= 20 && plen>= 2 && plen<= 20 ) {

            Matcher em = p.matcher(eName.trim());
            Matcher pm = p.matcher(ePass.trim());
         
            if (!em.find() && !pm.find()) {
                Employee emp = service.login(eName, ePass);
                if (emp != null) {
                    session.setAttribute("emp", emp);
                    /*model.addAttribute("list", employee);*/
                    return "main.jsp";
                }
                return "login.jsp";
            }
            return "login.jsp";
        }
        return "login.jsp";
    }

    /**
     * 判断账号存不存在
     * */
    @ResponseBody
    @RequestMapping("/eNameIf")
    public String eNameIf(String eName){
       String result = service.eNameIf(eName);
       if (result!=null){
           return "true";
       }
       return "false";
    }

    /**
     * 退出
     * */
    @RequestMapping("/logOut")
    public String logOut(HttpSession session){
        session.removeAttribute("emp");
        return "login.jsp";
    }

    /**
     * 修改密码
     * */
    @ResponseBody
    @RequestMapping("/updatePwd")
    public String updatePwd(String ePass ,HttpSession session){
        if (ePass != "" && ePass != null){
            int elen=ePass.trim().length();
            if (elen >= 6 && elen <= 20){
                String reg="^[A-Za-z0-9]+$";
                if (Pattern.matches(reg,ePass)){
                    Employee employee=(Employee) session.getAttribute("emp");
                    employee.setEPass(ePass);
                    int i = service.updatePwd(employee);
                    if (i > 0){
                        return "true";
                    }else {
                        return "false";
                    }
                }else {
                    return "false";
                }
            }else {
            return "false";
            }
        }else {
            return "false";
        }
    }

    /**
     * 分页查询
     * @param model
     * @param page
     * @param length
     * @param eStatus
     * @param realName
     * @return
     */
    @RequestMapping("/findEmpByPage")
    public String findEmpByPage(Model model,int page,int length,int eStatus,String realName){
        //1.根据员工状态查询总数
        int totalLine = service.selectCount(eStatus,realName);
        //总页数
        int totalPage = totalLine % length == 0 ? totalLine / length : totalLine / length + 1;
        //分页获取员工信息
        List<Employee> emps = service.findEmpByPage(eStatus,(page-1)*length,length,realName);
        //共享总行数、总页数、当前页、指定页数据
        model.addAttribute("totalLine",totalLine);
        model.addAttribute("totalPage",totalPage);
        model.addAttribute("page",page);
        model.addAttribute("emps",emps);
        //状态码、真实姓名
        model.addAttribute("eStatus",eStatus);
        model.addAttribute("realName",realName);

        return "user.jsp";
    }

    /**
     * 根据id删除员工信息
     * @param eid
     * @param eStatus
     * @return
     */
    @RequestMapping("/deleteUser")
    public String deleteUser(@RequestParam("eid") int eid,@RequestParam("eStatus") int eStatus){
        Employee employee = new Employee();
        employee.setEid(eid);
        employee.setEStatus(eStatus);
        service.deleteUser(employee);
        return "findEmpByPage?page=1&length=4&eStatus=0";
    }

    /**
     * 查询部门信息并跳转新增页面
     * @param model
     * @param eStatus
     * @return
     */
    @RequestMapping("/addUserFindInfo")
    public String addUserFindInfo(Model model,int eStatus){
        List<Depart> depts = departService.getAllDepart(eStatus);
        model.addAttribute("depts",depts);
        return "addUser.jsp";
    }

    /**
     * 新增信息
     * @param employee
     * @return
     */
    @RequestMapping("/addUser")
    public String addUser(Employee employee){
        Depart depart = departService.findDeptById(employee.getDid());
        employee.setDName(depart.getDName());
        service.addUser(employee);
        return "redirect:/findEmpByPage?page=1&length=4&eStatus=0";
    }

    /**
     * 修改信息
     * @param eid
     * @return
     */
    @RequestMapping("/findUserAndDepts")
    public String findUserAndDepts(int eid,Model model){
        Employee editEmp = service.findEmployeeById(eid);
        List<Depart> depts = departService.getAllDepart(editEmp.getEStatus());
        model.addAttribute("editEmp",editEmp);
        model.addAttribute("depts",depts);
        return "editUser.jsp";
    }

    @RequestMapping("/editUser")
    public String editUser(Employee employee){
        Depart depart = departService.findDeptById(employee.getDid());
        employee.setDName(depart.getDName());
        service.editUser(employee);
        return "redirect:/findEmpByPage?page=1&length=4&eStatus=0";
    }
}

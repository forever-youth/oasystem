package com.entity;

import lombok.Data;

import java.sql.Date;

/**
 * @author Admin
 */
@Data
public class Note {
    private int nid;
    private String title;
    private String context;
    private Date startDate;
    private Date endDate;
    private Float length;
    private Date subDate;
    private int eStatus;
    private Date relDate;
    private int eid;
}

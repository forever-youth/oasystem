package com.entity;

import lombok.Data;

import java.util.Date;

/**
 * @author Lenovo
 * 员工
 */
@Data
public class Employee {

    private int eid;
    private String eName;
    private String ePass;
    private String realName;
    private int eSex;
    private Date entryDate;
    private Date leaveDate;
    private int position;
    private int sal;
    private int eStatus;
    private int did;
    private String dName;

}

package com.entity;

import lombok.Data;

import java.util.Date;

/**
 * @author Lenovo
 */
@Data
public class Depart {
    private int dId;
    private String dName;
    private String duty;
    private Date creDate;
    private int dStatus;
}

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>无标题文档</title>
    <link href="css/style.css" rel="stylesheet" type="text/css"/>
    <script src="js/jquery-2.1.0.js"></script>
</head>

<body>
<div id="web">
    <p style="height:180px;"></p>
    <p align="center"><img src="img/logzi.png"/></p>
    <p style="height:40px;"></p>
    <div class="login">
        <div class="banner">
            <iframe id="frame_banner" src="sban/banner.html" height="218" width="100%" allowtransparency="true"
                    title="test" scrolling="no" frameborder="0"></iframe>
        </div>
        <form action="login" method="post" onsubmit="return checkSub()">
            <div class="logmain">
                <h1>&nbsp;</h1>
                <div class="logdv">
                    <span class="logzi">账 号：</span>
                    <input name="eName" type="text" id="ename" class="ename"/>
                </div>
                <div class="logdv">
                    <span class="logzi">密 码：</span>
                    <input name="ePass" type="password" id="epass" class="epass"/>
                </div>
                <div class="logdv">
                    <p class="logzi">&nbsp;</p>
                    <a href="#" class="more">忘记密码</a>
                    <input name="" type="checkbox" value="" class="cex"/>记住密码
                </div>
                <div class="logdv" style="height:40px;">
                    <p class="logzi">&nbsp;</p>
                    <input name="提交" type="submit" class="btnbg" value="点击登录"/>
                </div>

                <div>
                    <a href="#" class="more">注册</a>
                </div>

            </div>
        </form>
    </div>
    <p style="height:100px;"></p>
    <script>
        let bname = false;
        let bpass = false;
        $(function(){
            $("#ename").blur(function () {
                debugger
                //对账号进行验证
                const ename = $("#ename").val().trim();
                if (ename.length>=2&&ename.length<=20){
                    const pat = /[A-z]/;
                    if (!pat.test(ename)){
                        /*$("#nameExists").html("账号为大小写字母组成");*/
                        bname=false;
                    }else {
                        $.ajax({
                            type: "post",
                            url: "${pageContext.request.contextPath}/eNameIf",
                            data: {eName: ename},
                            success: function (result) {
                                if (result == "true") {
                                    /*$("#nameExists").html("");*/
                                    bname = true;
                                } else {
                                    /*$("#nameExists").html("用户不存在");*/
                                    bname = false;
                                }
                            }
                        })

                    }
                }else {
                    /*$("#nameExists").html("账号长度在2到20之间");*/
                    bname=false;
                }
            })


            $("#epass").blur(function () {
                debugger
                const pass = $("#epass").val().trim();

                if (pass.length >=2 && pass.length <= 20){
                    const pat = /[^A-z0-9]/;
                    if (!pat.test(pass)){
                        bpass=true;
                    }else {
                        bpass=false;
                    }
                }else {
                    bpass=false;
                }
            })
        })


        function checkSub() {
            debugger
            if (bname&&bpass){
                return true;
            }else {
                return false;
            }
        }
    </script>
</div>
</body>
</html>

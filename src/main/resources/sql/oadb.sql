/*
MySQL Backup
Database: oadb
Backup Time: 2021-10-24 21:38:22
*/

SET FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `oadb`.`depart`;
DROP TABLE IF EXISTS `oadb`.`employee`;
DROP TABLE IF EXISTS `oadb`.`note`;
CREATE TABLE `depart` (
                          `did` int(11) NOT NULL AUTO_INCREMENT,
                          `dname` varchar(50) NOT NULL,
                          `duty` varchar(255) NOT NULL,
                          `credate` date NOT NULL,
                          `dstatus` int(11) NOT NULL,
                          PRIMARY KEY (`did`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;
CREATE TABLE `employee` (
                            `eid` int(11) NOT NULL AUTO_INCREMENT,
                            `ename` varchar(50) NOT NULL,
                            `epass` varchar(50) NOT NULL,
                            `realname` varchar(50) NOT NULL,
                            `esex` int(11) NOT NULL,
                            `entrydate` date NOT NULL,
                            `leavedate` date DEFAULT NULL,
                            `position` int(11) NOT NULL,
                            `sal` int(11) NOT NULL,
                            `estatus` int(11) NOT NULL,
                            `did` int(11) DEFAULT NULL,
                            `dname` varchar(50) NOT NULL,
                            PRIMARY KEY (`eid`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;
CREATE TABLE `note` (
                        `nid` int(11) NOT NULL AUTO_INCREMENT,
                        `title` varchar(255) NOT NULL,
                        `context` varchar(255) NOT NULL,
                        `startdate` date NOT NULL,
                        `enddate` date NOT NULL,
                        `length` float NOT NULL,
                        `subdate` date NOT NULL,
                        `estatus` int(11) NOT NULL,
                        `reldate` date DEFAULT NULL,
                        `eid` int(11) DEFAULT NULL,
                        PRIMARY KEY (`nid`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
BEGIN;
LOCK TABLES `oadb`.`depart` WRITE;
DELETE FROM `oadb`.`depart`;
INSERT INTO `oadb`.`depart` (`did`,`dname`,`duty`,`credate`,`dstatus`) VALUES (1, 'boss', '老板，统领全局', '2021-10-14', 1),(2, '行政部', '负责传达领导通知，反馈各部门问题，打杂', '2021-10-14', 1),(3, '人事部', '负责公司员工招聘，培训', '2021-10-14', 0),(4, '开发部', '负责产品开发', '2021-10-14', 0),(5, '测试部', '负责产品测试', '2021-10-14', 0),(6, '市场部', '负责业务销售', '2021-10-14', 0),(7, '实施部', '负责产品部署和客户培训。', '2021-10-14', 1),(8, '部门', '你好', '2021-10-20', 1),(9, 'bum', '1', '2021-10-20', 0);
UNLOCK TABLES;
COMMIT;
BEGIN;
LOCK TABLES `oadb`.`employee` WRITE;
DELETE FROM `oadb`.`employee`;
INSERT INTO `oadb`.`employee` (`eid`,`ename`,`epass`,`realname`,`esex`,`entrydate`,`leavedate`,`position`,`sal`,`estatus`,`did`,`dname`) VALUES (1, 'he', '123456', '何猿猿', 0, '2021-10-14', NULL, 0, 50000, 0, 1, 'boss'),(2, 'hjma', '123456', '黄佳敏啊', 1, '2021-10-14', NULL, 1, 10000, 0, 6, '市场部'),(3, 'glx', '123456', '郭凌霞', 1, '2021-10-14', NULL, 1, 10000, 0, 3, '人事部'),(4, 'gq', '123456', '够呛', 0, '2021-10-14', NULL, 1, 20000, 0, 4, '开发部'),(5, 'ft', '123456', '符涛', 1, '2021-10-14', NULL, 1, 10000, 0, 5, '测试部'),(6, 'yc', '123456', '余灿', 0, '2021-10-14', NULL, 1, 10000, 0, 6, '市场部'),(7, 'lxf', '123456', '廖晓凤', 1, '2021-10-14', NULL, 1, 10000, 0, 7, '实施部'),(8, 'cgr', '123456', '陈桂荣', 0, '2021-10-14', NULL, 2, 10000, 0, 2, '行政部'),(9, 'csq', '123456', '陈树琦', 1, '2021-10-14', NULL, 2, 10000, 0, 2, '行政部'),(10, 'hhh', '123456', '胡鸿华', 0, '2021-10-14', NULL, 2, 10000, 0, 2, '行政部'),(11, 'llt', '123456', '雷龙腾', 1, '2021-10-14', NULL, 2, 10000, 0, 3, '人事部'),(12, 'pl', '123456', '彭雷', 0, '2021-10-14', NULL, 2, 10000, 0, 3, '人事部'),(13, 'tyl', '123456', '谭延龙', 1, '2021-10-14', NULL, 2, 10000, 0, 3, '人事部'),(14, 'wf', '123456', '王飞', 0, '2021-10-14', NULL, 2, 10000, 0, 4, '开发部'),(15, 'wk', '123456', '王康', 1, '2021-10-14', NULL, 2, 10000, 0, 4, '开发部'),(16, 'zj', '123456', '曾捷', 0, '2021-10-14', NULL, 2, 10000, 0, 4, '开发部'),(17, 'jl', '123456', '蒋靓', 1, '2021-10-14', NULL, 2, 10000, 0, 5, '测试部'),(18, 'lw', '123456', '刘雯', 0, '2021-10-14', NULL, 2, 10000, 0, 5, '测试部'),(19, 'wjy', '123456', '王嘉义', 1, '2021-10-14', NULL, 2, 10000, 0, 5, '测试部'),(20, 'wly', '123456', '文凌云', 0, '2021-10-14', NULL, 2, 10000, 0, 6, '市场部'),(21, 'llf', '123456', '李念峰', 1, '2021-10-14', NULL, 2, 10000, 0, 6, '市场部'),(22, 'lb', '123456', '罗斌', 0, '2021-10-14', NULL, 2, 10000, 0, 6, '市场部'),(23, 'lzy', '123456', '罗志勇', 1, '2021-10-14', NULL, 2, 10000, 0, 7, '实施部'),(24, 'scb', '123456', '苏成博', 0, '2021-10-14', NULL, 2, 10000, 0, 7, '实施部'),(25, 'sxw', '123456', '孙小炜', 1, '2021-10-14', NULL, 2, 10000, 0, 7, '实施部'),(26, 'new1', '123456', 'new1', 1, '2021-10-15', NULL, 2, 12000, 1, 3, '人事部'),(27, 'test0', '123456', 'test', 0, '2021-10-20', NULL, 2, 20000, 0, 3, '人事部');
UNLOCK TABLES;
COMMIT;
BEGIN;
LOCK TABLES `oadb`.`note` WRITE;
DELETE FROM `oadb`.`note`;
INSERT INTO `oadb`.`note` (`nid`,`title`,`context`,`startdate`,`enddate`,`length`,`subdate`,`estatus`,`reldate`,`eid`) VALUES (1, '事假', '结婚不上班', '2021-10-15', '2021-10-20', 5, '2021-10-15', 3, '2021-10-15', 1),(2, '事假', '回家种田', '2021-10-15', '2021-10-17', 2, '2021-10-15', 1, '2021-10-15', 2),(4, '病假啊', '不舒服啊', '2021-10-15', '2021-10-17', 1, '2021-10-15', 2, '2021-10-19', 1),(5, '病假', '不舒服', '2021-10-15', '2021-10-16', 1, '2021-10-15', 0, NULL, 1),(6, '病假', '不舒服', '2021-10-15', '2021-10-16', 1, '2021-10-15', 0, NULL, 1),(11, '没啥', '没啥', '2021-10-19', '2021-10-20', 1, '2021-10-20', 1, '2021-10-20', 1);
UNLOCK TABLES;
COMMIT;
